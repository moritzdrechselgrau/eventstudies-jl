using DataFrames, CSV, Arrow
using DataFramesMeta
using Random
using FixedEffectModels, EventStudies
using FreqTables
using Plots



# Real Test Data
# ========================================

data = copy(DataFrame(Arrow.Table("data/hrdata.arrow")))

# Define event (promotion)
data.move = data.rolelevel .> EventStudies.plag(data, :rolelevel, 1; by=:id, t=:period)
dropmissing!(data, :move)

# Stacked Event Study
settings_stack = (events=[:move], npre=2, npost=1, id=:id, period=:period, interactcontrols=true, extrapolate_pre=true, extrapolate_post=false, savedata=true, keep_leadslags=true, balance=true)

es_stack_none = stacked_eventstudy(data, @formula(ln_salary ~ fe(id) + fe(country)&fe(period)); bintype=:none, settings_stack...)
es_stack_sum = stacked_eventstudy(data, @formula(ln_salary ~ fe(id) + fe(country)&fe(period)); bintype=:sum, settings_stack...)
es_stack_dummy = stacked_eventstudy(data, @formula(ln_salary ~ fe(id) + fe(country)&fe(period)); bintype=:dummy, settings_stack...)

plot_stack = plot(legend=:topleft, ylabel="Log Salary")
plot!(es_stack_none, :move, c=1, label="stacked, no binning", legend=:topleft)
plot!(es_stack_sum, :move; c=2, label="stacked, bin (sum)", jitter=0.1)
plot!(es_stack_dummy, :move; c=3, label="stacked, bin (dummy)", jitter=0.1)
xlabel!("Years Since Role-Level Increase")


# Regular Event Study
settings= (events=[:move], npre=2, npost=1, id=:id, period=:period, extrapolate_pre=false, extrapolate_post=false, savedata=true, keep_leadslags=true, balance=true)

es_none = eventstudy(data, @formula(ln_salary ~ fe(id) + fe(country)&fe(period)); bintype=:none, settings...)
es_sum = eventstudy(data, @formula(ln_salary ~ fe(id) + fe(country)&fe(period)); bintype=:sum, settings...)
es_dummy = eventstudy(data, @formula(ln_salary ~ fe(id) + fe(country)&fe(period)); bintype=:dummy, settings...)

plot_reg = plot(legend=:topleft, ylabel="Log Salary")
plot!(es_none, :move; c=1, label="regular, no binning", jitter=0.1)
plot!(es_sum, :move; c=2, label="regular, bin (sum)", jitter=0.1)
plot!(es_dummy, :move; c=3, label="regular, bin (dummy)", jitter=0.1)
xlabel!("Years Since Role-Level Increase")

