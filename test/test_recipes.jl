using Revise
using Plots; theme(:bright)
using EventStudies
using DataFrames, CSV


data = CSV.read("5pc_testdata.csv", copycols=true)
data.lag_rolelevel_code = EventStudies._lags(data; var=:rolelevel_code, id=:id, period=:period, k=1)
data.lag_jobfam_code = EventStudies._lags(data; var=:jobfam_code, id=:id, period=:period, k=1)
data.switch_role = data.lag_rolelevel_code .!= data.rolelevel_code
data.switch_job = data.lag_jobfam_code .> data.jobfam_code

# Interaction settings
_interact = BinaryInteraction(var=:male, levels=false)

# Balanced event study
settings = EventStudySettings(npre=15, npost=5, tbase=[-2,-1], ytmin=670, ytmax=690, balance=true, population=false, dropfirst=true)
out = eventstudy(data, settings; y=:salary, events=[:switch_role], interact=_interact, control_terms=[], fe_terms=[fe(Term(:id)), fe(Term(:male))&fe(Term(:period)), fe(Term(:male))&fe(Term(:age))], id=:id, period=:period)

# Unbalanced event study
settings_ub = EventStudySettings(npre=15, npost=5, ytmin=670, ytmax=690, balance=false, population=false, dropfirst=true)
out_ub = eventstudy(data, settings_ub; y=:salary, events=[:switch_role], interact=_interact, control_terms=[], fe_terms=[fe(Term(:id)), fe(Term(:male))&fe(Term(:period)), fe(Term(:male))&fe(Term(:age))], id=:id, period=:period)

_aggperiods = EventStudies.aggregate_periods(out, 2)

# Plot
plt = plot()
plot!(plt, out, :switch_role_male, label="balanced")
plot!(plt, out, :switch_role_male, label="balanced", periods=_aggperiods)
plot!(plt, out_ub, :switch_role_male, label="unbalanced", markershape=:d)
vspan!(plt, [0,xlims(plt)[2]], color=:black, alpha = 0.1, label="")


EventStudies.aggregate_coef(out, :switch_role_male, [-15, -14:-10, -9:-2, 0:2, [3,4], 5])
EventStudies.aggregate_stderror(out, :switch_role_male, [-15, -14:-10, -9:-2, 0:2, [3,4], 5])

EventStudies._average_coef(out, :switch_role_male, -15:-13)
EventStudies._average_stderror(out, :switch_role_male, [-15,-14,-13])
EventStudies._average_stderror(out, :switch_role_male, -2)
EventStudies._average_coef(out, :switch_role_male, -2:-1)

EventStudies.average_periods(out, [-15,-14:-10])
EventStudies.aggregate_periods(out, 2)

collect(Base.Iterators.partition(1:0,2))
x1 = zeros(10,10)
_s = 1:2:9
x2 = ones(5,5)
x1[_s,_s] .= x2
x1

sqrt(sum(Σ) / n²)
