using DataFrames, DataFramesMeta, CSV
using EventStudies
using Test
using Plots

# Data
# TODO: add source (run Siggi's do-file to generate bf2017_es.dta, keep if year <= 2011, keep only relevant variables)
df = DataFrame(CSV.File("testdata/schmidheiny_siegloch.csv"))

# Formula (without event)
ff = @formula(ln_GJSI ~ frac_total_ui + fe(yearmonth) + fe(state))

# Event study
npre = 3
npost = 4
events = [:B_PBD_incr]
rr = eventstudy(df, ff; events=events, npre=npre, npost=npost, id=:state, period=:yearmonth)

# Test results
# TODO: make this a test in the EventStudies.jl package
@test round(rr.reg.coef[end], digits=5) ≈ 6.23609
@test round(stderror(rr.reg)[end], digits=5) ≈ 2.57398

# Accessing coefficients, standard errors and confidence intervals (with added base levels)
@test coef(rr, events[1]; addbase=true)[3] ≈ 0.0
@test isnan(stderror(rr, events[1]; addbase=true)[3])
@test all(isnan.(confint(rr, events[1]; addbase=true)[3,:]))

# Plots
p = plot(rr, events[1]; significance=0.05, markerstrokecolor=:auto)
@test p.series_list[1].plotattributes[:x] == -npre:npost
@test p.series_list[1].plotattributes[:y] == coef(rr, events[1]; addbase=true)

p2 = plot(rr, events[1], events[1]; significance=0.05, markerstrokecolor=:auto)
