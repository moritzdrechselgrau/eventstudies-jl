clear all


global npre = 3
global npost = 3
global ytmin = 1
global ytmax = 10 + $npre + $npost

* create panel
set obs $ytmax
generate id = 1
generate period = _n
expand 2, generate(_exp)
replace id = 2 if _exp == 1
drop _exp
xtset id period

* indata
generate indata = 1 if id == 1, after(period)
replace indata = (period > $npost + 1) & (period < $T - $npre ) if id == 2
label define yesno 1 "yes" 0 "no"
label values indata yesno

* counter
bysort id indata (period): generate counter = _n if indata == 1
xtset id period
order counter, after(period)

* reverse counter
gsort id indata -period
by id indata: generate rcounter = _n if indata == 1
xtset id period
order rcounter, after(counter)

* outcome
generate y = runiform() + id if indata

* event
generate event = 0
replace event = 1 if id == 1 & period == 7
replace event = 1 if id == 2 & period == 7
replace event = . if indata == 0
label values event yesno

* pre
foreach k of numlist $npre(-1)1 {
	generate pre`k' = f`k'.event if indata
}
gsort id -period
by id: replace pre$npre = sum( pre$npre ) if indata & !missing(pre$npre )
xtset id period
* post
foreach k of numlist 1/$npost {
	generate post`k' = l`k'.event if indata
}
by id: replace post$npost = sum( post$npost ) if indata & !missing(post$npost )
* now
generate now = event, after(pre1)

* recode missing at boundaries
replace pre$npre = 0 if rcounter == $npre
replace post$npost = 0 if counter == $npost

* complete case
egen complete = rowmiss(y pre* post* now) 
replace complete = complete == 0
label values complete yesno


* Use knowledge on population
* ============================================

foreach var of varlist pre* now post* {
	generate _`var' = `var', after(`var')
}
foreach k of numlist 1/$npre {
	replace _pre`k' = 0 if indata == 1 & rcounter <= `k' & period < $ytmax - $npre + 1
}
foreach k of numlist 1/$npost {
	replace _post`k' = 0 if indata == 1 & counter <= `k' & period > $ytmin + $npost
}

* complete case
egen _complete = rowmiss(y _pre* _post* _now) 
replace _complete = _complete == 0
label values _complete yesno

sum *complete if indata == 1




