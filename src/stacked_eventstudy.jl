"""
  stacked_eventstudy( df::DataFrame, f::FormulaTerm; 
                      events, controlevents=missing, 
                      id::Symbol, period::Symbol,
                      npre::Int, npost::Int, tbase::Int=-1, 
                      extrapolate_post::Bool=false, extrapolate_pre::Bool=false, 
                      bintype=:none, balance::Bool=true, cluster=missing, interactcontrols::Bool=true, 
                      verbose::Bool=true, savedata::Bool=false, keep_leadslags::Bool=false,
                      kwargs...)
"""
function stacked_eventstudy(df::DataFrame, f::FormulaTerm; 
                            events, controlevents=missing, iscontrol::Union{Missing,Symbol,String}=missing, istreated::Union{Missing,Symbol,String}=missing,
                            id::Symbol, period::Symbol,
                            npre::Int, npost::Int, tbase::Int=-1, 
                            extrapolate_post::Bool=false, extrapolate_pre::Bool=false, 
                            bintype=:none, balance::Bool=false, cluster=missing,
                            verbose::Bool=true, savedata::Bool=false, keep_leadslags::Bool=false, 
                            kwargs...)
	
  if verbose
		println("\n"^2, "="^60, "\n", "STACKED EVENT STUDY ANALYSIS", "\n", "="^60, "\n")
	end

  
  # Processing Inputs & Checks
  # ===============================================

  # Copy data
  df = copy(df)
  
  # Checks
  @assert npost >= 0
  @assert npre >= 0
  
  # Clustering
  cluster = ismissing(cluster) ? Vcov.cluster(id) : cluster


  # Construct Event Variables (Leads and Lags)
  # ===============================================
  #! Do we have to bin the endpoints in the stacked event study with multiple events per unit?
  
  # Construct leads and lags of events
  # ----------------------------------------------
  prevars = [Vector{Symbol}(undef,npre) for e in events]
  postvars = [Vector{Symbol}(undef,npost+1) for e in events]

  for (i,e) in enumerate(events)
    out_e = construct_leadslags!(df, e; period=period, id=id, 
                                        npre=npre, npost=npost,
                                        extrapolate_pre=extrapolate_pre,
                                        extrapolate_post=extrapolate_post, 
                                        bintype=bintype)
    prevars[i] = out_e.prevars
    postvars[i] = out_e.postvars
  end

  # List of event variables
  # ----------------------------------------------
  eventvars_all = Symbol[]
  eventvars_nobase = Symbol[]
  for (i,e) in enumerate(events)
    eventvars_i = [prevars[i]; postvars[i]]
    eventvars_i_nobase = drop_baseperiod(eventvars_i; npre=npre, npost=npost, tbase=tbase)
    push!(eventvars_all, eventvars_i...)
    push!(eventvars_nobase, eventvars_i_nobase...)
  end


  # Stacked Data: Build a dataset for each event time
  # ====================================================

  # possible event times
  any_event = replace(sum(eachcol(df[!,events])) .> 0, missing => false)
  event_times = sort(unique(df[any_event,period]))
  tmin, tmax = extrema(event_times)

  # select eligible events
  if !extrapolate_pre
    filter!(t -> t >= tmin + npre, event_times)
  end
  if !extrapolate_post
    filter!(t -> t <= tmax - npost, event_times)
  end
  n_windows = length(event_times)
  @assert !isempty(event_times)
  println("Event cohorts: $event_times")

  # Initiate stacked data
  df_stacked = DataFrame()

  for (it,t) in enumerate(event_times)

    # Select observations in event window
    # ---------------------------------------

    # restrict to t-specific event window
    df_t = copy(@subset(df, t - npre .<= df[!,period] .<= t + npost))

    # balance data in event window
    if balance
      balance_data!(df_t; id=id)
    end

  
    # Treated units
    # ----------------------------
    
    # indicator for having one or more events
    df_t.n_events = ismissing(istreated) ? sum(eachcol(df_t[!,events])) .> 0 : copy(df_t[!,istreated])
    df_t.any_event = df_t.n_events .> 0

    # drop units with multiple treatment events in the event window
    transform!(groupby(df_t, id), :n_events => sum => :n_events)
    @rsubset!(df_t, :n_events .<= 1)

    # continue with next cohort if no valid units
    if nrow(df_t) == 0
      println("Event cohort $t: no valid observations")
      continue
    end

    # units with an event at time t
    df_t.treated = df_t.any_event .* (df_t[!,period] .== t)
    transform!(groupby(df_t, id), :treated => (x -> maximum(x) == 1) => :treated)

    # Control units
    # ----------------------------

    if typeof(controlevents) <: AbstractVector
    
    # indicator for having one or more control events
      df_t.n_controlevents = sum(eachcol(df_t[!,controlevents]))
      df_t.any_controlevent = df_t.n_controlevents .> 0

      # drop units with multiple control events in the event window
      transform!(groupby(df_t, id), :n_controlevents => sum => :n_controlevents)
      @rsubset!(df_t, :n_controlevents .<= 1)

      # units with a control event at time t
      df_t.control = df_t.any_controlevent .* (df_t[!,period] .== t)
      transform!(groupby(df_t, id), :control => (x -> maximum(x) == 1) => :control)

      # drop auxiliary columns
      select!(df_t, Not([:n_controlevents, :any_controlevent]))

    elseif !ismissing(iscontrol)

      # indicator for having control status
      df_t.n_notcontrol = df_t[!,iscontrol] .== false

      # keep units with iscontrol throughout the event window
      transform!(groupby(df_t, id), :n_notcontrol => sum => :n_notcontrol)
      df_t.control = df_t.n_notcontrol .== 0

      # drop auxiliary columns
      select!(df_t, Not(:n_notcontrol))

    else

      # units with no event in the window
      df_t.control = df_t.n_events .== 0

    end

    # drop auxiliary columns
    select!(df_t, Not([:n_events, :any_event]))
    
    # Append to the stacked data
    # -------------------------------------
    
    # Keep only valid treatment and control observations
    @subset!(df_t, .|(:treated, :control))

    # cohort variable and relative time
    df_t.cohort = fill(t, nrow(df_t))
    df_t.reltime = df_t[!,period] .- t

    # append to stacked data
    append!(df_stacked, df_t)

    # Report
    n_treated_units = length(unique(df_t[df_t.treated .== 1, id]))
    n_control_units = length(unique(df_t[df_t.control .== 1, id]))
    n_treated_obs = length(df_t[df_t.treated .== 1, id])
    n_control_obs = length(df_t[df_t.control .== 1, id])
    println("Event cohort $t: $n_treated_obs treated observations ($n_treated_units units), $n_control_obs control observations ($n_control_units units)")


  end

  # Regression
  # ===============================================

  # Terms for RHS
  rhs_terms = AbstractTerm[]
  push!(rhs_terms, [term(evar) for evar in eventvars_nobase]...)
  push!(rhs_terms, InteractionTerm((fe(term(:reltime)), fe(term(:cohort)))))
  push!(rhs_terms, InteractionTerm((fe(term(:treated)), fe(term(:cohort)))))
  push!(rhs_terms, f.rhs...)

  # Regression Formula
  formula = FormulaTerm(f.lhs, Tuple(rhs_terms))

  # keep only needed variables
  vars = [id, period]
  push!(vars, events...)
  if !ismissing(controlevents) push!(vars, controlevents...) end
  if !ismissing(iscontrol) push!(vars, iscontrol) end
  if !ismissing(istreated) push!(vars, istreated) end
  push!(vars, StatsModels.termvars(formula.lhs)...)
  for t in formula.rhs
    push!(vars, StatsModels.termvars(t)...)
  end
  select!(df_stacked, unique(vars))
  @show unique(vars)

  # Regression
  results = nothing
  try
    results = reg(df_stacked, formula, cluster; kwargs...)
  catch e
    throw(e)
    return formula, df_stacked
  end
  println("\n", results)
  
  
  # Output
  # =================================================
  
  # Data
  if !keep_leadslags
    select!(df_stacked, Not(eventvars_nobase))
  end
  df_stacked.esample = copy(results.esample)
  outdata = savedata ? df_stacked : missing

  # settings
  settings=(; events, controlevents, npre, npost, tbase, extrapolate_post, extrapolate_pre, bintype, balance)

  EventStudy(reg=results, data=outdata, settings=settings)

end