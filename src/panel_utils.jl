# ===========================================================
# Leads and Lags
# ===========================================================

function plags!(df::AbstractDataFrame, cols, k; by=Symbol[], t=:period)

    # parse inputs
    k = Int.([k;])
    cols = String.([cols;])

    # copy relevant data to shift time variable
    dfk = copy(select(df, Symbol.([by; t; cols])))
    truetime = copy(dfk[!,t])

    # names of leads/lags
    names_ll = getnames_leadslags(cols, k)

    # drop leads/lags that already exist
    if any(names_ll .∈ Ref(names(df)))
      select!(df, Not(intersect(names(df), names_ll)))
    end

    # loop over leads/lags
    for _k in k
        # shift time variable
        dfk[!,t] = truetime .+ _k
        # assign name and merge to main data
        names_ll_k = getnames_leadslags(cols, _k)
        rename!(dfk, cols .=> names_ll_k)
        leftjoin!(df, dfk, on=[by; t])
        rename!(dfk, names_ll_k .=> cols)
    end
end


function plag(df::AbstractDataFrame, col, k::Int=1; by=Symbol[], t)
  dfk = copy(select(df, unique(Symbol.([by; t; col]))))
  dfk[!,t] .+= k
  dfk.lag = copy(df[!,col])
  dfout = copy(select(df, [by;t]))
  leftjoin!(dfout, dfk, on=[by; t]).lag
end
plead(df::AbstractDataFrame, col, k::Int=1; kwargs...) = plag(df, col, -k; kwargs...)

function plag!(df::AbstractDataFrame, col, k::Int=1; t, name=_getname_leadlag(col,k), by=Symbol[])
  dflag = select(df, Symbol.([by; t; col]))
  dflag[!,t] .+= k
  rename!(dflag, Symbol(col) => Symbol(name))
  if name in names(df) select!(df, Not(name)) end
  leftjoin!(df, dflag, on=[by; t])
end
plead!(df::AbstractDataFrame, col, k::Int=1; name=_getname_leadlag(col,k), kwargs...) = plag!(df, col, -k; name=name, kwargs...)


"""
    Get the name of a lead/lag variable constructed by, e.g. 'plags!'.
    Name convention: 1st lag of 'col' will be 'L1_col' and 2nd lead of 'col' will be 'F2_col'.
"""
function getnames_leadslags(cols, k::Union{Int, Vector{Int}, UnitRange})
    @assert issorted(k) "The elements of 'k' must be sorted."
    list_names = String[]
    for col in [cols;]
        for _k in [k;]
            push!(list_names, _getname_leadlag(col, _k))
        end
    end
    list_names
end
function _getname_leadlag(var::Union{Symbol, String}, k::Int)
    FL = k < 0 ? "F" : "L"
    "$(FL)$(abs(k))_$(var)"
end



# ===========================================================
# Counter Variables
# ===========================================================

function set_counter!(df::DataFrame; countervar::Symbol, periodvar::Symbol, idvar::Symbol=:id)
  @assert issorted(df, [idvar, periodvar])
  df[!,countervar] = _count(df, idvar)
end

""" Create counter variable """
@inline function _count(df::DataFrame, var::Symbol)
  t = 1
  _var = df[!,var]
  counter = missings(Int,nrow(df))
  for i in 1:nrow(df)
    if i == 1 || _var[i] == _var[i-1]
      counter[i] = t
    else
      t = 1
      counter[i] = t
    end
    t += 1
  end
  counter
end
""" Create reverse counter variable """
@inline function _rcount(df::DataFrame, var::Symbol)
  t = 1
  _var = df[!,var]
  counter = missings(Int,nrow(df))
  for ii in reverse(1:nrow(df))
    if ii == nrow(df) || _var[ii] == _var[ii+1]
      counter[ii] = t
    else
      t = 1
      counter[ii] = t
    end
    t += 1
  end
  counter
end


# ===========================================================
# Dates and Time
# ===========================================================

""" Convert periods defined by separate year and month to running t variable """
@inline function get_period(year::Int, month::Int; year0::Int=1960, month0::Int=1)
  ((year - year0) * 12) + (month - month0)
end

""" Convert period-string numeric period variable """
@inline function get_period(period_str::String; kwargs...)
  year = parse(Int64, period_str[1:4])
  month = parse(Int64, period_str[6:end])
  get_period(year, month; kwargs...)
end

@inline function get_year(period::Int; year0::Int=1960)
  div(period, 12) + year0
end
@inline function get_month(period::Int; year0::Int=1960, month0::Int=1)
  month = period - 12*div(period, 12) + month0
end
