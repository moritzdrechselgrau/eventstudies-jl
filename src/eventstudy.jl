"""
  eventstudy(data::DataFrame, settings::EventStudySettings; 
              events, controlevents=missing, 
              id::Symbol, period::Symbol,
              npre::Int, npost::Int, tbase::Int=-1, 
              extrapolate_post::Bool=false, extrapolate_pre::Bool=false, 
              bintype=:none, balance::Bool=true, cluster=missing, interactcontrols::Bool=true, 
              verbose::Bool=true, savedata::Bool=false, keep_leadslags::Bool=false,
              dropmultiple::Bool=false,
              kwargs...)
"""
function eventstudy(df::AbstractDataFrame, f::FormulaTerm; 
                    events, id::Symbol, period::Symbol,
                    npre::Int, npost::Int, tbase::Union{Int, Vector{Int}, UnitRange}=-1, 
                    aggregate_periods::Union{Nothing, Vector{<:Pair}}=nothing,
                    extrapolate_post::Bool=false, extrapolate_pre::Bool=false, 
                    bintype=:sum, cluster=missing,
                    verbose::Bool=true, savedata::Bool=false, keep_leadslags::Bool=false,
                    omit::Union{Pair, Vector{<:Pair}, Nothing}=nothing, 
                    subset::Union{Nothing, AbstractVector} = nothing,
                    kwargs...)


    if verbose
        println("\n"^2, "="^60, "\n", "EVENT STUDY ANALYSIS", "\n", "="^60, "\n")
    end


    # Processing Inputs & Checks
    # ===============================================

    # Checks
    @assert npost >= 0
    @assert npre >= 0

    # Event variables
    if verbose println("Event variables: ", events) end
    events = [events; ]

    # Clustering
    cluster = ismissing(cluster) ? Vcov.cluster(id) : cluster

    # Omitted variables
    if !isnothing(omit)
        omit = [omit;]
        @assert all([isa(pair[1], String) * isa([pair[2];], AbstractVector) for pair in omit]) "Keyword argument 'omit' must be (a vector of) pair(s) mapping a string (event name) to an integer or vector of integers (lead/lag indices)."
    end

    # Data
    # ===============================================

    # Copy data
    # keepvars = Symbol.([id; period; events; StatsModels.termvars(f.rhs); collect(cluster.clusternames)])
    # df = select(df, keepvars)
    df = copy(df)
    # TODO: Should we make this optional?

    # Sort data
    if !issorted(df, [id, period])
        if verbose println("Sorting data ...") end
        sort!(df, [id, period])
    end

    # drop if events are missing
    # TODO: does this make sense? 
    #! This cannot handle npost == 0
    nrow_old = nrow(df)
    if !isnothing(subset)
        subset = subset[completecases(df, events)]
    end
    dropmissing!(df, events)
    nrow_new = nrow(df)
    if verbose println("Dropped $(nrow_old - nrow_new) observations with missing events.") end


    # Leads and Lags of Event Variables
    # ===============================================

    # Construct leads and lags from -npre to npost (without tbase)
    list_leadslags, summary_extplt = construct_leadslags!(df, events; period=period, id=id, 
                                                npre=npre, npost=npost, tbase=tbase,
                                                extrapolate_pre=extrapolate_pre,
                                                extrapolate_post=extrapolate_post, 
                                                bintype=bintype, verbose=verbose, omit=omit)

    # Option: Aggregate Leads and Lags
    if !isnothing(aggregate_periods)
        if verbose println("Aggregating leads and lags ...") end
        list_leadslags = aggregate_leadslags!(df, events, aggregate_periods, list_leadslags)
        # adjust npre, npost, tbase
        npre = abs(min(0, minimum([k[2] for k in aggregate_periods])))
        npost = max(0, maximum([k[2] for k in aggregate_periods]))
        tbase = [-1]
        # aggregate_periods = nothing
        #! HARDCODED !!!!
    end

    # Regression
    # ===============================================

    # Terms for RHS
    rhs_terms = term.(list_leadslags)
    if typeof(f.rhs) <: AbstractTerm
        rhs_terms = [rhs_terms; term(f.rhs)]
    else 
        rhs_terms = [rhs_terms; term.(f.rhs)...]
    end
    # TODO: Splatting will not work if f.rhs only contains a single term

    # Regression Formula
    formula = FormulaTerm(f.lhs, Tuple(rhs_terms))

    # Regression
    if verbose println("Running regression ...") end
    results = reg(df, formula, cluster; subset=subset, kwargs...)
    println("\n", results)

    # Output
    # =================================================

    # Output estimation data
    if !keep_leadslags
        select!(df, Not(list_leadslags))
    end
    @subset!(df, results.esample)
    outdata = savedata ? df : nothing

    # settings
    settings=(; events, npre, npost, tbase, aggregate_periods, extrapolate_post, extrapolate_pre, bintype)

    # info
    info = extrapolate_post || extrapolate_pre ? (; summary_extplt,) : nothing

    # return
    EventStudy(reg=results, data=outdata, settings=settings, info=info)

end
