module EventStudies

# ===================================================================
# Dependencies
# ===================================================================

using Reexport
using Parameters
using StatsBase
using LinearAlgebra
using Random
using RecipesBase
# using RegressionTables
using Formatting
using Distributions
using DataFrames, DataFramesMeta
using CSV # for loading test data
@reexport using FixedEffectModels


# ===================================================================
# Load files
# ===================================================================

# include("leadslags.jl")
include("panel_utils.jl")
include("structures.jl")
include("construct_leadslags.jl")
include("eventstudy.jl")
# include("stacked_eventstudy.jl") # TODO: needs updating
include("postestimation.jl")
include("plot_recipes.jl")

# ========================================================================
# Exported methods and types
# ========================================================================

export EventStudy
export eventstudy #, stacked_eventstudy
export construct_leadslags!
export plag, plead, plag!, plead!, plags!

end # module
