# ===========================================================
# Leads and Lags
# ===========================================================

@inline function _lag(i::Int, var::Vector, id::Vector, period::Vector, k::Int=1)
  out = missing
  if k == 0
    out = var[i]
  else
    for j in reverse(1:k)
      if (i>j) && (id[i-j] == id[i]) && (period[i]-period[i-j] == k)
        out = var[i-j]
        break
      end
    end
  end
  out
end

function _lags(df::DataFrame; var::Symbol, id::Symbol, period::Symbol, k::Int=1, check_sorted::Bool=true)
  if check_sorted && !issorted(df,[id,period])
    @error "Data not sorted w.r.t. $id and $period."
  end
  _var = df[!,var]
  _id = df[!,id]
  _period = df[!,period]
  out = missings(eltype(_var), nrow(df))
  for i in 1:nrow(df)
    out[i] = _lag(i, _var, _id, _period, k)
  end
  out
end

function _sumlags(df::DataFrame; var::Symbol, id::Symbol, period::Symbol, counter::Symbol, k_vec, check_sorted::Bool=true)
  if check_sorted && !issorted(df,[id,period])
    @error "Data not sorted w.r.t. $id and $period."
  end
  _var = df[!,var]
  _id = df[!,id]
  _period = df[!,period]
  _counter = df[!,counter]
  _k_vec = sort(k_vec)
  _outtype = eltype(_var) in [Bool, Union{Missing,Bool}] ? typeof(2*true) : eltype(_var)
  out = missings(_outtype, nrow(df))
  for i in 1:nrow(df)
    out[i] = _sumlags(i, _var, _id, _period, _counter, _k_vec)
  end
  out
end

function _sumlags(i, var::Vector, id::Vector, period::Vector, counter::Vector, k_vec)
  if all(k_vec .>= counter[i])
    return missing
  else
    out = zero(eltype(var))
    for k in k_vec
      if k < counter[i]
        out += _lag(i, var, id, period, k)
        if ismissing(out)
          return out
        end
      else
        return out
      end
    end
    return out
  end
end

@inline function _lead(i::Int, var::Vector, id::Vector, period::Vector, k::Int=1)
  out = missing
  if k == 0
    out = var[i]
  else
    obs = length(var)
    for j in reverse(1:k)
      if (i <= obs-j) && (id[i+j] == id[i]) && (period[i+j]-period[i] == k)
        out = var[i+j]
        break
      end
    end
  end
  out
end

function _leads(df::DataFrame; var::Symbol, id::Symbol, period::Symbol, k::Int=1, check_sorted::Bool=true)
  if check_sorted && !issorted(df,[id,period])
    @error "Data not sorted w.r.t. $id and $period."
  end
  _var = df[!,var]
  _id = df[!,id]
  _period = df[!,period]
  out = missings(eltype(_var), nrow(df))
  for i in 1:nrow(df)
    out[i] = _lead(i, _var, _id, _period, k)
  end
  out
end

function _sumleads(df::DataFrame; var::Symbol, id::Symbol, period::Symbol, counter::Symbol, k_vec, check_sorted::Bool=true)
  if check_sorted && !issorted(df,[id,period])
    @error "Data not sorted w.r.t. $id and $period."
  end
  _var = df[!,var]
  _id = df[!,id]
  _period = df[!,period]
  _revcounter = sort(df[!,[id,counter]], [id,counter], rev = [false,true])[!,counter]
  _k_vec = sort(k_vec)
  _outtype = eltype(_var) in [Bool, Union{Missing,Bool}] ? typeof(2*true) : eltype(_var)
  out = missings(_outtype, nrow(df))
  for i in 1:nrow(df)
    out[i] = _sumleads(i, _var, _id, _period, _revcounter, _k_vec)
  end
  out
end

function _sumleads(i, var::Vector, id::Vector, period::Vector, revcounter::Vector, k_vec)
  if all(k_vec .>= revcounter[i])
    return missing
  else
    out = zero(eltype(var))
    for k in k_vec
      if k < revcounter[i]
        out += _lead(i, var, id, period, k)
        if ismissing(out)
          return out
        end
      else
        return out
      end
    end
    return out
  end
end
