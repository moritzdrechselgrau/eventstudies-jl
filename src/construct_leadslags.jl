"""
    construct_leadslags!(df::AbstractDataFrame, events::Vector; period, id, npre::Int, npost::Int, tbase=tbase, extrapolate_pre::Bool=false, extrapolate_post::Bool=false, bintype::Symbol=:sum)

    Construct `npre` leads and `npost` lags of `events` while skipping the lead(s)/lag(s) in `tbase`. The highest order lead and lag are binned. Extrapolate out-of-sample leads and lags if `extrapolate_pre = true` or `extrapolate_post = true`.
"""
function construct_leadslags!(df::AbstractDataFrame, events::Vector; period, id, npre::Int, npost::Int, tbase=-1, extrapolate_pre::Bool=false, extrapolate_post::Bool=false, bintype::Symbol=:sum, verbose::Bool=false, omit::Union{Pair, Vector{<:Pair}, Nothing}=nothing)

    # Checks
    @assert bintype in [:sum, :dummy, :none] "Keyword argument bintype must be one of :sum, :dummy, or :none."

    # set of lead and lag orders
    k = setdiff(-npre:npost, tbase)
    
    # efficiently construct leads and lags
    if verbose println("Constructing leads and lags ...") end
    plags!(df, events, k; t=period, by=id) 

    # Binning at the end points of the restricted event window
    if bintype != :none
        dummy = bintype == :dummy
        if verbose println("Binning largest leads and lags (type: $bintype) ...") end
        bin_leadslags!(df, events; npre=npre, npost=npost, tbase=tbase, by=id, t=period, dummy=dummy)
    else
        if verbose println("Not binning largest leads and lags (strange choice)") end
    end

    # Extrpolate leads and lags (if out of sample)
    summary_ext = DataFrame()
    if extrapolate_post
        if verbose println("Extrapolating post events (lags) ...") end
        ext_post = extrapolate_lags!(df, events, npost; tbase=tbase, by=id, t=period)
        summary_ext = vcat(ext_post, summary_ext)
    end
    if extrapolate_pre
        if verbose println("Extrapolating pre events (leads) ...") end
        ext_pre = extrapolate_leads!(df, events, npre; tbase=tbase, by=id, t=period)
        summary_ext = vcat(ext_pre, summary_ext)
    end

    # set omitted variables to zero
    if !isnothing(omit)
        for (event, ks) in omit
            for k in [ks;]
                FL = k < 0 ? "F$(abs(k))" : "L$(abs(k))"
                var = "$(FL)_$(event)"
                df[!, var] .= 0
            end
        end
    end  

    # Column names of leads and lags
    names_leadslags = getnames_leadslags(events; npre=npre, npost=npost, tbase=tbase)

    # Output
    return names_leadslags, summary_ext

end

function bin_leadslags!(df::DataFrame, events::Vector; npre::Int, npost::Int, tbase=-1, by=:id, t=:period, dummy::Bool=false)
    @assert issorted(df, [by; t])
    k = setdiff(-npre:npost, tbase)
    if any(k .< 0) && -npre ∉ [tbase;]
      binlargestlead!(df, events, npre; by=by, t=t, dummy=dummy)
    end
    if any(k .>= 0) && npost ∉ [tbase;]
      binlargestlag!(df, events, npost; by=by, t=t, dummy=dummy)
    end
end


"""
    binlargestlag!(df::DataFrame, events::Vector, pre::Int; by=:id, t=:period)

    Sum over the largest lag of each respective event variable.
"""
function binlargestlag!(df::DataFrame, events::Vector, npost::Int; by=:id, t=:period, dummy::Bool=false)
    periods = select(groupby(df, by), t, t => minimum => :firstperiod)
    sumrows = df[!,t] .> periods.firstperiod .+ max(npost - 1, 0)
    Lbin = missings(Int, nrow(df))
    for event in events
        eventvar = "L$(npost)_$(event)"
        Lbin .= missings(Int, nrow(df))
        startpoints = periods.firstperiod .+ max(npost - 1, 0) .== df[!,t]
        if npost == 0
            Lbin[startpoints] .= df[startpoints, eventvar]
        else
            Lbin[startpoints] .= 0
        end
        for i in 1:nrow(df)
            if sumrows[i]
                if df[i, t] == df[i-1, t] + 1
                    Lbin[i] = Lbin[i-1] + df[i, eventvar]
                else
                    Lbin[i] = missing
                end
            end
        end
        df[!, eventvar] .= Lbin
        if dummy
            df[!, eventvar] .= Lbin .> 0
        end
    end
end

"""
    binlargestlead!(df::DataFrame, events::Vector, pre::Int; by=:id, t=:period)

    Sum over the largest lead of each respective event variable.
"""
function binlargestlead!(df::DataFrame, events::Vector, npre::Int; by=:id, t=:period, dummy::Bool=false)
    periods = select(groupby(df, by), t, t => maximum => :lastperiod)
    sumrows = df[!,t] .< periods.lastperiod .- (npre - 1)
    Fbin = missings(Int, nrow(df))
    for event in events
        Fbin .= missings(Int, nrow(df))
        Fbin[periods.lastperiod .- max(npre - 1, 0) .== df[!,t]] .= 0
        eventvar = "F$(npre)_$(event)"
        for i in nrow(df):-1:1
            if sumrows[i]
                if df[i, t] == df[i+1, t] - 1
                    Fbin[i] = Fbin[i+1] + df[i, eventvar]
                else
                    Fbin[i] = missing
                end
            end
        end
        df[!, eventvar] .= Fbin
        if dummy
            df[!, eventvar] .= Fbin .> 0
        end 
    end
end


"""
    extrapolate_leads!(df::AbstractDataFrame, events::Vector, npre::Int; by, t)

    Extrapolate the leads of the events in `events` to zero if they cannot be observed. Return a DataFrame with the share of extrapolated leads and the average value of the event variable among non-extrapolated observations.
"""
function extrapolate_leads!(df::AbstractDataFrame, events::Vector, npre::Int; tbase=-1, by, t)
    lastperiod = select(groupby(df, by), t => maximum => :x).x
    js = abs.(sort(setdiff(-npre:-1, tbase)))
    n_coef = length(js)
    out = DataFrame([-js zeros(n_coef, length(events)+1)], ["k"; "share_extplt"; "mean_" .* events])
    for (i,j) in enumerate(js)
        _extrapolate = df[!,t] .> lastperiod .- j
        out[i,"share_extplt"] = mean(_extrapolate) 
        for (ievent, event) in enumerate(events)
        var = Symbol("F$(j)_", event)
        out[i,2+ievent] = mean(skipmissing(df[_extrapolate .== false, var]))
        df[_extrapolate, var] .= 0
        end
    end
    sort!(out, :k)
end

"""
    extrapolate_lags!(df::AbstractDataFrame, events::Vector, npost::Int; by, t)

    Extrapolate the lags of the events in `events` to zero if they cannot be observed.
"""
function extrapolate_lags!(df::AbstractDataFrame, events::Vector, npost::Int; tbase=-1, by, t)
    firstperiod = select(groupby(df, by), t => minimum => :x).x
    js = setdiff(1:npost, tbase)
    n_coef = length(js)
    out = DataFrame([js zeros(n_coef, length(events)+1)], ["k"; "share_extplt"; "mean_" .* events])
    for (i,j) in enumerate(js)
        _extrapolate = df[!,t] .< firstperiod .+ j
        out[i,"share_extplt"] = mean(_extrapolate) 
        for (ievent, event) in enumerate(events)
        var = Symbol("L$(j)_", event)
        out[i,2+ievent] = mean(skipmissing(df[_extrapolate .== false, var]))
        df[_extrapolate, var] .= 0
        end
    end
    sort!(out, :k)
end

"""
    aggregate_leadslags!(df::AbstractDataFrame, events, aggregate_periods::Vector{<:Pair})

    Aggregate leads and lags of the events in `events` according to the pairs in `aggregate_periods`. The first element of each pair are the original leads/lags and the second element is the new lead/lag. The new lead/lag is the sum of the original leads/lags. The original lead/lags are dropped from the DataFrame.
"""
function aggregate_leadslags!(df::AbstractDataFrame, events, aggregate_periods::Vector{<:Pair}, llnames_orig::Vector)


    # check that there are no duplicate new leads/lags and that they are integers
    k_newagg = [pair[2] for pair in aggregate_periods]
    llnames_newagg = getnames_leadslags(events, k_newagg)
    @assert typeof(k_newagg) <: Vector{Int} "The second element of each pair in keyword argument 'aggregate_periods' must be an integer."
    @assert allunique([pair[2] for pair in aggregate_periods]) "There are duplicate new leads/lags in keyword argument 'aggregate_periods'."

    # check that each lead/lag appears at most once
    llnames_orig_agg = getnames_leadslags(events, vcat([pair[1] for pair in aggregate_periods]...))
    @assert allunique(llnames_orig_agg) "There are duplicate original leads/lags in keyword argument 'aggregate_periods'."


    # auxiliary variable for sum of leads/lags
    ll_sum = missings(Float64, nrow(df))

    # loop over events
    llnames_drop = String[]
    for event in events
        for pair in aggregate_periods

            # set sum to zero
            ll_sum .= zero(eltype(0.0))

            # sum over original leads/lags
            kfrom = [pair[1];]
            for k in kfrom
                namefrom = _getname_leadlag(event, k)
                ll_sum .+= df[!, namefrom]
                push!(llnames_drop, namefrom)
            end

            # copy into new lead/lag (with _temp suffix)
            kto = pair[2]
            nameto_temp = _getname_leadlag(event, kto)
            df[!, nameto_temp * "_temp"] = copy(ll_sum)
            
        end
    end

    # drop original leads/lags
    select!(df, Not(llnames_orig))

    # remove _temp suffix from new leads/lags
    rename!(df, llnames_newagg .* "_temp" .=> llnames_newagg)

    # names of leads and lags
    return sort_leadslags(llnames_newagg, events)

end


"""
    getnames_leadslags

    Get the names of the leads and lags of the events in `events`.
"""
function getnames_leadslags(es::EventStudy, event)
    @unpack npre, npost, tbase, aggregate_periods = es.settings
    if isnothing(aggregate_periods)
        getnames_leadslags(event; npre=npre, npost=npost, tbase=tbase)
    else
        getnames_leadslags(event, aggregate_periods; npre=npre, npost=npost, tbase=tbase)
    end
end

function getnames_leadslags(events; npre::Int, npost::Int, tbase=-1)
    getnames_leadslags(events, setdiff(-npre:npost, tbase))
end
getnames_leadslags(events, ::Nothing; kwargs...) = getnames_leadslags(events; kwargs...)

function getnames_leadslags(events, aggregate_periods::Vector{<:Pair}; kwargs...)
    
    list_names = getnames_leadslags(events, [pair[2] for pair in aggregate_periods])
    
    @assert allunique(list_names) "There are duplicate names in the list of leads/lags."

    sort_leadslags(list_names, events)
end

function sort_leadslags(names::Vector{<:Union{Symbol,String}}, events)
    out = String[]
    for event in events
        names_event = filter(x -> occursin(event, x), string.(names))
        prefix = replace.(names_event, "_$(event)" => "")
        k = [(x[1] == 'F' ? -1 : 1) * parse(Int, x[2:end]) for x in prefix]
            #! careful: x[1] == "F" is not the same as x[1] == 'F' (String vs. Char)
        names_event = names_event[sortperm(k)]
        push!(out, names_event...)
    end
    out
end

function getperiods_leadslags(npre, npost, tbase, aggregate_periods=nothing)
    if isnothing(aggregate_periods)
        setdiff(-npre:npost, tbase)
    else
        periods_new = [pair[2] for pair in aggregate_periods]
        sort(periods_new)
    end
end
function getperiods_leadslags(es::EventStudy)
    @unpack npre, npost, tbase, aggregate_periods = es.settings
    getperiods_leadslags(npre, npost, tbase, aggregate_periods)
end
