function lincom_event(es::EventStudy, Revent::NamedTuple; average_periods=nothing, nan2zero::Bool=false, cumulative::Bool=false)

    # construct R (each row is one event time)
    R = construct_R_lincom_event(es, Revent; cumulative=cumulative)

    # average over event times if specified
    if !isnothing(average_periods)
        rows_periods = unique(get_ihorizon.(Ref(es), [average_periods;]))
        R = sum(R[rows_periods, :], dims=1) / length(rows_periods)
    end

    # compute lin. comb. of coefficients and standard errors
    _coef = coef(es)
    _vcov = nan2zero ? replace(vcov(es), NaN => 0.0) : vcov(es)
    lincom_coefs = R * _coef
    lincom_se = sqrt.(diag(R * _vcov * R'))

    (coef=lincom_coefs, se=lincom_se)
end

function construct_R_lincom_event(es::EventStudy, Revent::NamedTuple; cumulative::Bool=false)

    @unpack npre, npost = es.settings

    # number of coefficients per event
	n_coef = length(coef(es))
    n_coef_event = length(getperiods_leadslags(es)) # npre + npost + 1
    events = string.(keys(Revent))

    # construct R
    R = zeros(n_coef_event, n_coef)
    for (i, event) in enumerate(events)
        coefindices_i = _findcoef(es, event)
        for ic in 1:n_coef_event
            if cumulative
                if ic <= npre
                    for j in ic:npre
                        R[ic,coefindices_i[j]] = Revent[i]
                    end
                else
                    for j in npre+1:ic
                        R[ic,coefindices_i[j]] = Revent[i]
                    end
                end
            else
                R[ic,coefindices_i[ic]] = Revent[i]
            end
        end
    end
    return R
end


function get_ihorizon(es::EventStudy, horizon::Int)
    if isnothing(es.settings.aggregate_periods)
        ito = horizon
    else
        period_agg_mapping = collect_aggregate_periods(es)
        ifrom = findfirst(period_agg_mapping.from .== horizon)
        ito = period_agg_mapping.to[ifrom]
    end
    return findfirst(EventStudies.getperiods_leadslags(es) .== ito)
end


function collect_aggregate_periods(es::EventStudy)
    from = Int[]
    to = Int[]
    for pair in es.settings.aggregate_periods
        push!(from, pair[1]...)
        push!(to, fill(pair[2], length(pair[1]))...)
    end
    return (; from, to)
end