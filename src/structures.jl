@with_kw struct EventStudy
	reg::FixedEffectModel
  	data::Union{Nothing,DataFrame}=nothing
	settings::NamedTuple
	info::Union{Nothing,NamedTuple}=nothing
end

# Event Window
_effectwindow(npre::Int, npost::Int) = -npre:npost
_effectwindow(s::NamedTuple) = _effectwindow(s.npre, s.npost)


# Extend functions from FixedEffectModels
FixedEffectModels.coef(es::EventStudy) = FixedEffectModels.coef(es.reg)
FixedEffectModels.coefnames(es::EventStudy) = FixedEffectModels.coefnames(es.reg)
FixedEffectModels.stderror(es::EventStudy) = FixedEffectModels.stderror(es.reg)
FixedEffectModels.vcov(es::EventStudy) = FixedEffectModels.vcov(es.reg)
FixedEffectModels.nobs(es::EventStudy) = FixedEffectModels.nobs(es.reg)
FixedEffectModels.dof_residual(es::EventStudy) = FixedEffectModels.dof_residual(es.reg)
FixedEffectModels.r2(es::EventStudy) = FixedEffectModels.r2(es.reg)
FixedEffectModels.adjr2(es::EventStudy) = FixedEffectModels.adjr2(es.reg)
FixedEffectModels.islinear(es::EventStudy) = FixedEffectModels.islinear(es.reg)
FixedEffectModels.deviance(es::EventStudy) = FixedEffectModels.deviance(es.reg)
FixedEffectModels.rss(es::EventStudy) = FixedEffectModels.rss(es.reg)
FixedEffectModels.mss(es::EventStudy) = FixedEffectModels.mss(es.reg)
FixedEffectModels.confint(es::EventStudy) = FixedEffectModels.confint(es.reg)
FixedEffectModels.predict(es::EventStudy, df::AbstractDataFrame) = FixedEffectModels.predict(es.reg, df)
FixedEffectModels.residuals(es::EventStudy, df::AbstractDataFrame) = FixedEffectModels.residuals(es.reg, df)
FixedEffectModels.residuals(es::EventStudy) = FixedEffectModels.residuals(es.reg)
FixedEffectModels.fe(es::EventStudy) = FixedEffectModels.fe(es.reg)
FixedEffectModels.coeftable(es::EventStudy) = FixedEffectModels.coeftable(es.reg)
# FixedEffectModels.show(io::IO, es::EventStudy) = FixedEffectModels.show(io, es.reg)


"""Get coefficients for `event`"""
function FixedEffectModels.coef(es::EventStudy, event::Union{Symbol,String}; addbase::Bool=false)
	out = coef(es)[_findcoef(es, event)]
	addbase ? add_baselevel(out, es; baseval=0.0) : out
end
"""Get standard errors of coefficients for `event`"""
function FixedEffectModels.stderror(es::EventStudy, event::Union{Symbol,String}; addbase::Bool=false)
	out = stderror(es)[_findcoef(es, event)]
	addbase ? add_baselevel(out, es; baseval=NaN) : out
end
"""Get confidence intervals of coefficients for `event`"""
function FixedEffectModels.confint(es::EventStudy, event::Union{Symbol,String}; addbase::Bool=false)
	out = confint(es)[_findcoef(es, event),:]
	addbase ? add_baselevel(out, es; baseval=NaN) : out
end


"""Get standard errors of a linear combination of coefficients"""
function FixedEffectModels.stderror(reg::FixedEffectModel, R::Vector)
	sqrt(R' * reg.vcov * R)
end
function FixedEffectModels.vcov(reg::FixedEffectModel, R::Matrix)
	@assert size(R, 2) == size(reg.vcov, 1)
	R * reg.vcov * R'
end



"""Find indices of coefficients for `event` in the full coefficient vector `allnames`"""
function _findcoef(allnames::Vector, event, npre, npost, tbase, aggregate_periods=nothing)
	eventnames = getnames_leadslags([event;], aggregate_periods; npre=npre, npost=npost, tbase=tbase)
	[findfirst(name .== allnames) for name in eventnames]
end
_findcoef(reg::FixedEffectModel, args...) = _findcoef(coefnames(reg), args...)
_findcoef(es::EventStudy, event) = _findcoef(coefnames(es), event, es.settings.npre, es.settings.npost, es.settings.tbase, es.settings.aggregate_periods)


"""
	add_baselevel(x::AbstractVector; npre, npost, tbase, baseval=0.0)

	Add base level (default: zero) to a vector or matrix `x`. `x` is assumed to be appropriately ordered.
"""
function add_baselevel(x::AbstractVector{<:Real}, t::AbstractVector{<:Int}, tbase; baseval=0.0, cumulative=false)
	if cumulative
		xsort = x[sortperm(t)]
		baseval += xsort[t .< tbase][end]
	end
	xnew = [x; fill(baseval, length(tbase))]
	xnew[sortperm([t; tbase])]
end
function add_baselevel(x::AbstractMatrix{<:Real}, t, tbase; kwargs...)
	hcat([add_baselevel(x[:,i], t, tbase; kwargs...) for i in 1:size(x,2)]...)
end
function add_baselevel(x::AbstractVector{<:Real}; npre::Int, npost::Int, tbase, aggregate_periods=nothing, kwargs...)
	add_baselevel(x, getperiods_leadslags(npre, npost, tbase, aggregate_periods), tbase; kwargs...)
end
function add_baselevel(x::AbstractMatrix{<:Real}; kwargs...)
	hcat([add_baselevel(x[:,i]; kwargs...) for i in 1:size(x,2)]...)
end
function add_baselevel(x, es::EventStudy; kwargs...)
	@assert size(x, 1) == length(getperiods_leadslags(es))
	add_baselevel(x, getperiods_leadslags(es), es.settings.tbase; kwargs...)
end