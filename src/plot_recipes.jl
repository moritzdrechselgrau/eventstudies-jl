@recipe function plot(es::EventStudy, event::Union{Symbol,String}; significance=0.05, nan2zero=true, cumulative=false)
  @unpack npre, npost, tbase = es.settings

  # Series
  time = sort([getperiods_leadslags(es); tbase])
  i_tbase = findfirst(time .== tbase)
  Revent = NamedTuple{(Symbol(event),)}((1,))
  lincom = lincom_event(es, Revent; nan2zero=nan2zero, cumulative=cumulative)
  beta = add_baselevel(lincom.coef, es; baseval=0.0, cumulative=cumulative)
  beta .-= beta[i_tbase]
  se = add_baselevel(lincom.se, es; baseval=NaN)
  ci = se * Distributions.quantile(Normal(), 1 - significance/2)

  # Plot Options
  err --> (ci, ci)
  markershape --> :o
  xlabel --> "Periods Since Event"
  label --> string(event)

  time, beta
end

@recipe function plot(es::EventStudy, Revent::NamedTuple; significance=0.05, nan2zero=true, select=nothing, cumulative=false)
  @unpack npre, npost, tbase = es.settings

  time = sort([getperiods_leadslags(es); tbase])
  lincom = lincom_event(es, Revent; nan2zero=nan2zero, cumulative=cumulative)

  if isnothing(select)
    select = 1:length(time)
  end
  
  time = time
  i_tbase = findfirst(time .== tbase)
  beta = add_baselevel(lincom.coef, es; baseval=0.0, cumulative=cumulative)
  # beta .-= beta[i_tbase]

  time = time[select]
  beta = beta[select]

  se = add_baselevel(lincom.se, es; baseval=NaN)[select]
  ci = se * Distributions.quantile(Normal(), 1 - significance/2)

  label = ""
  for (i,event) in enumerate(keys(Revent))
    s = Revent[i] >= 0 ? " + " : " - "
    if i == 1 && Revent[i] >= 0
      s = ""
    end
    a = abs(Revent[i]) == 1 ? "" : "$(abs(Revent[i])) "
    label *= s * a * string(event)
  end

  err --> (ci, ci)
  markershape --> :o
  xlabel --> "Periods Since Event"
  label --> label
  # legend --> true

  time, beta
end

# Subtract two sets of event coefficients (difference plot)
@recipe function plot(es::EventStudy, event1::Union{Symbol,String}, event2::Union{Symbol,String}; significance=0.05)
  @unpack tbase = es.settings

  time = sort([getperiods_leadslags(es); tbase])
  beta = coef(es, event1; addbase=false) - coef(es, event2; addbase=false)

  _select1 = _findcoef(es, event1)
  _select2 = _findcoef(es, event2)

  se = zeros(length(beta))
  for i in 1:length(_select1)
    R = zeros(length(es.reg.coef))
    R[_select1[i]] = 1
    R[_select2[i]] = -1
    se[i] = stderror(es.reg, R)
  end
  beta = add_baselevel(beta, es; baseval=0.0)
  se = add_baselevel(se, es; baseval=NaN)

  ci = se * Distributions.quantile(Normal(), 1 - significance/2)

  err --> (ci, ci)
  markershape --> :o
  xlabel --> "Periods Since Event"
  label --> "$event1 - $event2"
  # legend --> true

  time, beta
end

